package codeGenerator;

	import initial_Settings::*;
	import prn1::*;
	import BRAMCore::*;
	import remainder::*;
	import Vector::*;

	interface CodeGeneratorIfc;
		method Action initializePRN();
		method Action readData(Int#(8) prn1, Int#(8) downByFactor1);
		method Action readReqBRAM(Int#(16) j);
		method Bit#(2) readFromBRAM();
	endinterface

	module mkCode(CodeGeneratorIfc);
		Remainder_Ifc rem1 <- mkRem;

		Reg#(Int#(8)) prn <- mkReg(0);
		Reg#(Int#(8)) downByFactor <- mkReg(0);

		Reg#(Bit#(1)) initFlag <- mkReg(0);
		Reg#(Bit#(2)) flag <- mkReg(0);
		Reg#(Int#(16)) i <- mkReg(0);
		Reg#(Int#(64)) temp <- mkReg(0);

		Vector#(1023,Reg#(Bit#(2))) prnCode <- replicateM(mkReg(1));
		BRAM_PORT#(Int#(16),Bit#(2)) code <- mkBRAMCore1(12000,False);
		
		rule r1 (initFlag == 1 && flag == 0 && i <= 11999);
			Int#(64) x = (1023*(signExtend(i)+1))/12000;
			rem1.remInput(signExtend(x),1023);
			flag <= 1;
		endrule
		rule r2 (flag == 1);
			let temp1 = rem1.returnData();
			temp <= temp1;
			flag <= 2;
		endrule

		rule r3 (flag == 2);
			let temp1 = prnCode[temp];
			code.put(True,i,temp1);
			flag <= 0;
			i <= i+1;
		endrule

		method Action initializePRN() if(initFlag == 0);
			prnCode[5] <= -1;
			prnCode[64] <= -1;
			prnCode[133] <= -1;
			prnCode[197] <= -1;
			prnCode[240] <= -1;
			prnCode[311] <= -1;
			prnCode[367] <= -1;
			prnCode[499] <= -1;	
			//$display("Method r0");
			initFlag <= 1;
		endmethod

		method Action readData(Int#(8) prn1, Int#(8) downByFactor1);
			prn <= prn1;
			downByFactor <= downByFactor1;
		endmethod

		method Action readReqBRAM(Int#(16) j);
			code.put(False,j,?);
		endmethod

		method Bit#(2) readFromBRAM();
			let x = code.read();

			return x;
		endmethod
	endmodule // mkCode

	module mkTb();
		CodeGeneratorIfc ifc <- mkCode;

		Reg#(Bit#(2)) flag <- mkReg(0);

		rule x1(flag == 0);
			ifc.readData(1,1);
			flag <= 1;
		endrule
		rule x2(flag == 1);
			ifc.initializePRN();
			flag <= 2;
		endrule
		rule x3(flag == 2);
			ifc.readReqBRAM(100);
			flag <= 3;
		endrule
		rule x4(flag == 3);
			let lv = ifc.readFromBRAM();
			$display("Value = %d",lv);
			$finish(0);
		endrule
	endmodule // mkTb

endpackage : codeGenerator
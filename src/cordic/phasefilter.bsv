/*
Module name: Phase Filter
Description: This module computes the second order phase filter after phase discriminator in PLL
			 Input : Previous two phases from discriminator and the last phase from the NCO during
			 		 the current tracking iteration
			 Output: Phase to the nco for the next tracking iteration
			 The output phase is computed as kp1*phase_in1 + kp2*phase_in2, where
			 	kp1=1;
				phase_in1= Previous phase shift from discriminator
				phase_in2= Cirrent phase shift from NCO in tracking
//TODO is a register required at the output?
*/

package phasefilter;

    import Real::*;

    interface Ifc_phasefilter;
        method Action get_inputs(Int#(33) phase_in1,Int#(33) phase_in2);
        method Int#(33) results();
    endinterface : Ifc_phasefilter

    module mkphasefilter(Ifc_phasefilter);

        Reg#(Bit#(1)) rg_readFlag <- mkReg(0);
        Reg#(Int#(2)) rg_kp1 <- mkReg(1);
        Reg#(Int#(2)) rg_kp2 <- mkReg(0);
        Reg#(Int#(33)) rg_phase_out <- mkReg(0);

        method Action get_inputs(Int#(33) phase_in1,Int#(33) phase_in2) if(rg_readFlag == 0);
            Int#(35) a = (signExtend(rg_kp1)*signExtend(phase_in1))+(signExtend(rg_kp2)*signExtend(phase_in2));
            rg_phase_out <= truncate(a);
            rg_readFlag <= 1;
        endmethod
        method Int#(33) results() if(rg_readFlag == 1);
            return rg_phase_out;
        endmethod

    endmodule : mkphasefilter
endpackage : phasefilter

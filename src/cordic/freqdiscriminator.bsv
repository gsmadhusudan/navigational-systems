package freqdiscriminator;

    import Real ::*;
    import FixedPoint::*;

    interface Ifc_freqdiscriminator;
        method Action get_inputs(Int#(33) phase_in1, Int#(33) phase_in2,Int#(5) integration_time);
        method FixedPoint#(16,32) results();
    endinterface : Ifc_freqdiscriminator

    module mkfreqdiscriminator(Ifc_freqdiscriminator);

        Reg#(Bit#(1)) rg_readFlag <- mkReg(0);
        Reg#(Bit#(3)) rg_flag <- mkReg(0);

        Reg#(Int#(33)) rg_phase1 <- mkReg(0);
        Reg#(Int#(33)) rg_phase2 <-mkReg(0);
        Reg#(Int#(5)) rg_int_time <- mkReg(0);
        Reg#(FixedPoint#(16,32)) rg_freq_out <- mkReg(0);

        rule r1_subtractor((rg_readFlag==1)&&(rg_flag==0));
            Int#(33) a = rg_phase1-rg_phase2;//phase1 is the latest phase and phase2 is the phase output from the phase discriminator during previous iteration
            Int#(10) b = 500;
            Int#(10) c = b/zeroExtend(rg_int_time);
            FixedPoint#(33,32) d = fromInt(a);
            FixedPoint#(16,32) e = fromInt(c);
            FixedPoint#(16,32) f = fxptTruncate(d >> 31);
            rg_freq_out <= f*e;
            rg_flag <= 1;
            rg_readFlag <= 0;
        endrule

        method Action get_inputs(Int#(33) phase_in1, Int#(33) phase_in2,Int#(5) integration_time) if(rg_readFlag == 0);
            rg_phase1 <= phase_in1;
            rg_phase2 <= phase_in2;
            rg_int_time <= integration_time;
            rg_readFlag <= 1;
        endmethod
        method FixedPoint#(16,32) results() if(rg_flag == 1);
            return rg_freq_out;
        endmethod

    endmodule : mkfreqdiscriminator
endpackage : freqdiscriminator
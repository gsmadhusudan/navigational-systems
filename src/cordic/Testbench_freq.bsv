	import freqsynthesiser ::*;
	import Vector ::*;
	import Real::*;
	import FixedPoint::*;

module mkTestbench();

	Reg#(Int#(8)) frqbin_index <- mkReg(20);
	Reg#(Int#(16)) interval <- mkReg(500);
	Reg#(Int#(32)) samp_freq <- mkReg(12000000);//12 MHz
	Reg#(FixedPoint#(16,32)) base_freq <- mkReg(-10000); //-10 KHz
	Reg#(Int#(33)) phase <- mkReg(0);
	Reg#(Bit#(3)) flag <- mkReg(0);
	Reg#(Int#(33)) angle <- mkReg(0);
	
	Ifc_freqsynthesiser ifc_tb <- mkfreqsynthesiser;
	
	rule r1_give_inputs(flag == 0);
		ifc_tb.get_inputs(interval, samp_freq,base_freq,phase);
		flag <= 1;
	endrule
	
	rule r2_get_results(flag == 1);
		let a = ifc_tb.results_base();
		let b = ifc_tb.results_interval();
		$display("\n base angle = %d",a);
		$display("\n Interval angle = %d",b);
		$finish;
	endrule
endmodule : mkTestbench
//This package is an FSM for Lockdetectors and Delta-Pseudorange measurement
//

package lockdetectorfsm;

    import Real::*;
    import FixedPoint::*;
    import BRAM::*;
    import BRAMCore::*;
    import ClientServer::*;
    import GetPut::*;
    import StmtFSM::*;

    import trackingfsm::*;
    import signchangecompute::*;
    import navdatacompute::*;

    interface Ifc_lockdetectorfsm;
        method Action getinputs(Bit#(20) loopcnt, Bit#(1) signchange);
        method Action from_interrupt(Bit#(2) cnr);
        method Bit#(2) result_status();
        method Action readbram(Bit#(11) address);// Total number of entires is 1500 (50*30)
        method ActionValue#(Bit#(1)) nav_data_out;
    endinterface : Ifc_lockdetectorfsm

    function BRAMRequest#(Bit#(5), Int#(64)) makeRequest(Bool write, Bit#(5) addr, Int#(64) data);
        return BRAMRequest{
        write: write,
        responseOnWrite:False,
        address: addr,
        datain: data
        };
    endfunction

    module mklockdetectorfsm(Ifc_lockdetectorfsm);

        Reg#(Bit#(20)) rg_loopcnt <- mkReg(0);
        Reg#(Bit#(6)) rg_mod_loopcnt <- mkReg(0);
        Reg#(Bit#(2)) rg_CNR <- mkReg(0);
/*if rg_CNR == 00, CNR>threshold and imag(CNR) = 0;
  if rg_CNR == 01, CNR>threshold and imag(CNR)!= 0; 
  if rg_CNR == 10, CNR<threshold and imag(CNR) = 0; 
  if rg_CNR == 11, CNR<threshold and imag(CNR)!= 0*/
        Reg#(Bit#(5)) rg_signchangeI <- mkReg(0);
        Reg#(Bit#(5)) rg_signchangeQ <- mkReg(0);
        Reg#(Bit#(2)) rg_trackingstatus <- mkReg(0);
/*if rg_trackingstatus == 0, there is a lock, next tracking iteration can be performed;
  if rg_trackingstatus == 1, hot start is given (fine frequency tuning)
  if rg_trackingstatus == 2, warm start is given (procedure similar to acquisition)
  if rg_trackingstatus == 3, lock is lost, no further tracking*/
        Reg#(Bit#(11)) rg_lockcount <- mkReg(0);
        Reg#(Bit#(1)) rg_signchange <- mkReg(0);
        Reg#(Bit#(20)) rg_databoundary <- mkReg(0);
        Reg#(Bit#(1)) readFlag <- mkReg(0);
        Reg#(Bit#(1)) flag <- mkReg(0);

        BRAM_Configure cfg1 = defaultValue;
        BRAM1Port#(Bit#(11),Bit#(1)) nav_data <- mkBRAM1Server(cfg1);
        Reg#(Bit#(11)) rg_count <- mkReg(0);

        Ifc_trackingfsm ifc1 <- mktrackingfsm;
        Ifc_signchangecompute ifc2 <- mksignchangecompute;
        Ifc_navdatacompute ifc3 <- mknavdatacompute;

        Stmt lockdetect_stmt = 
        (seq
            //Checking whether the lock is lost or sustained
            rg_mod_loopcnt <= rg_loopcnt%40;
            if(rg_loopcnt%20==0);
            seq
                par
                    action//CNR estimation as an interrupt to processor
                        delay(1);
                    endaction
                    seq//sign change in I and Q data
                        ifc2.initialise(rg_mod_loopcnt);
                        action
                            rg_signchangeI <= ifc2.result_signI();
                            rg_signchangeQ <= ifc2.result_signQ();
                        endaction
                    endseq
                endpar
                if((rg_CNR == 0)&&(rg_signchangeI <= 2)&&(rg_signchangeQ <= 2)) //Next 20 tracking iterations can be done
                action
                    rg_trackingstatus <= 0;
                    rg_lockcount <= rg_lockcount + 1;
                endaction
                else if(rg_loopcnt == 20)
                        rg_trackingstatus <= 2;//warm start
                     else if(rg_loopcnt > 60)
                            rg_trackingstatus <= 3;//lock is lost
                          else
                            rg_trackingstatus <= 1;//hot start
            endseq
            else
            action
                rg_trackingstatus <= 0;
            endaction
            //Data boundary calculation
            if((rg_lockcount > 5)&&(rg_signchange == 1)&&(rg_databoundary==0))//if rg_signchange is 1, then there is a sign change.
                rg_databoundary <= rg_loopcnt;
            if((rg_databoundary>0)&&((rg_loopcnt-rg_databoundary)%20==0))
            seq
                ifc3.getinputs(rg_mod_loopcnt);
                Bit#(1) temp = ifc3.result();
                nav_data.portA.request.put(makeRequest(True,rg_count,temp));
                rg_count <= rg_count+1;
            endseq
            if(rg_count == 1500)
                rg_count <= 0;
            readFlag <= 0;
            flag <= 1;
        endseq);

        FSM lockdetect_fsm <- mkFSM(lockdetect_stmt);

        rule r0_initialise((lockdetect_fsm.done)&&(readFlag==1));
            lockdetect_fsm.start;
        endrule

        method Action getinputs(Bit#(20) loopcnt, Bit#(1) signchange) if(readFlag == 0);
            rg_loopcnt <= loopcnt;
            rg_signchange <= signchange;
            readFlag <= 1;
            flag <= 0;
        endmethod
        method Action from_interrupt(Bit#(2) cnr);
            rg_CNR <= cnr;
        endmethod
        method Bit#(2) status()if(flag == 1);
            return rg_trackingstatus;
        endmethod
        method Action readbram(Bit#(11) address);
            nav_data.portA.request.put(makeRequest(False,address,?));
        endmethod
        method ActionValue#(Bit#(1)) nav_data_out;
            let data_val <- nav_data.portA.response.get;
            return data_val;    
        endmethod
    endmodule : mklockdetectorfsm

endpackage : lockdetectorfsm
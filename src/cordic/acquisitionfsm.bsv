/*
Module: Acquisition FSM
Description: This module implements the Acquisition FSM. The state machine is as follows:
	1. Generate scaled versions of the frequencies which are required for freq. shifting.
	   In the first cycle, the first input (-10kHz) is sent to the freq synthesizer.
	2. In the second cycle, the base angle and the interval angle are read from the freq
	   synthesizer, and are sent to the freq shift block. This block would then read the sample data
	   and perform freq. shifting on each sample.
	3. Third cycle onwards, for the next `samp_freq number of cycles, the outputs of the freq shift
	   block are read, and are fed to the code shift module, which in turn does the correlation.
	4. Once correlation is done for this particular freq shift, the freq shift module is again
	   called to perform freq shift on the sample data for the next freq shift.
	5. The next cycle onwards the new freq shifted data is sent to the code shift block.
	6. This happens untill all freq. shift have been performed.
	7. ?
	
To be included : 
1)Code shift correlator, 
2)If the number of channels is less than the number of satellites
*/
package acquisitionfsm;

import StmtFSM::*;
import Real::*;
import BRAM::*;
import BRAMCore::*;
import GetPut::*;
import ClientServer::*;
import FixedPoint::*;
import freqsynthesiser::*;
import cordicip::*;
//import codeshifter::*;

typedef 10 Samples;
typedef 500 Interval;
typedef 12000000 Sampling_frequency;
typedef 10000 Stop_frequency;

interface Ifc_acquisitionfsm;
    method Action getting_started();
    method Bit#(1) prn();
    method Bit#(22) code_shifter;
    method Int#(16) freq_shifter;
endinterface : Ifc_acquisitionfsm

module mkacquisitionfsm(Ifc_acquisitionfsm);
    Integer samp1 = valueOf(Samples);
    Bit#(16) samp = fromInteger(samp1);
    Integer interval1 = valueOf(Interval);
    Int#(16) interval = fromInteger(interval1);
    Integer samp_freq1 = valueOf(Sampling_frequency);
    Int#(32) samp_freq = fromInteger(samp_freq1);
    Integer stop_freq = valueOf(Stop_frequency);
    Int#(16) stop = fromInteger(stop_freq);

    Ifc_freqsynthesiser freq_synth <- mkfreqsynthesiser;
    Ifc_cordicip cordic <- mkcordicip;
    //Ifc_codeshifter ifc3 <- mkcodeshifter;

    Reg#(Bit#(6)) iter <- mkReg(0);
    Reg#(Bit#(16)) i <- mkReg(0);
    Reg#(Bit#(6)) j <- mkReg(0);
    Reg#(Bit#(1)) readFlag <- mkReg(0);
    Reg#(Bit#(1)) flag <- mkReg(0);
    Reg#(Bit#(1)) rg_start <- mkReg(0);
    Reg#(Bit#(1)) prn_out <- mkReg(0);
    Reg#(Bit#(22)) code_shift <- mkReg(0);
    Reg#(Int#(16)) freq_shift <- mkReg(0);

    Stmt acq_stmt = 
    (seq
        action // Calculation of number of iterations
            Bit#(16) a = fromInteger((2*stop_freq)/interval1);      //10kHz
            FixedPoint#(16,32) start = -fromInteger(stop_freq);     //-10KHz. Using Fixed Point because of filter parameters introduced in the tracking FLL where the 
            freq_synth.get_inputs(interval,samp_freq,start,0);      //Generates scaled version of the frequencies
            iter <= truncate(a)+1;                                  //Value that specifies number of freq. bins
        endaction
        noAction;
        action // Getting the scalled version of base angle and interval angle
            Int#(33) a = freq_synth.results_base();
            Int#(33) b = freq_synth.results_interval();
            cordic.initialise(a,b,iter);
        endaction
        noAction;
        for(j<=0; j<iter; j<=j+1) // to perform for calculated number of frequency shifts
            par
                for(i<=0; i<=samp; i<=i+1) // to read all the frequency shifted values
                    par
                        action
                            if(i<samp)
                                cordic.readbram(i); //providing address to read from BRAM
                            if(i==0)
                                readFlag <= 1;
                        endaction
                        if(readFlag == 1)
                        action
                            let cosine <- cordic.bramcosine; //reading I' value from BRAM
                            let sine <- cordic.bramsine; //reading Q' value from BRAM
                            $display($time," %d,%d : From testbench : I_out = %d and Q_out = %d \n",j,i-1,cosine,sine);
                        endaction
                    endpar
                    //CODE SHIFT CORRELATOR COMES HERE
            endpar
            cordic.next_iter();//As all the values are read, providng command to start next frequency shift
            readFlag <= 0;
            //prn_out <= 1;//This will be set once the satellite is acquired
            endseq
            flag <= 1;
            rg_start<=0;
    endseq);

    FSM acq_fsm <- mkFSM(acq_stmt);

    rule r0_initialise((rg_start==1)&&(acq_fsm.done));
        acq_fsm.start;
    endrule

    method Action getting_started if(rg_start == 0);
        rg_start <= 1;
    endmethod
    method Bit#(1) prn() if(flag == 1);
        return prn_out;
    endmethod
    method Bit#(22) code_shifter if(flag == 1);
        return code_shift;
    endmethod
    method Int#(16) freq_shifter if(flag == 1);
        return freq_shift;
    endmethod

endmodule : mkacquisitionfsm
endpackage : acquisitionfsm
